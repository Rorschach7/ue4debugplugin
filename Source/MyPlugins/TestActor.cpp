// Fill out your copyright notice in the Description page of Project Settings.


#include "TestActor.h"
#include "DebugLib.h"
#include "MyPlugins.h"

// Sets default values
ATestActor::ATestActor() {
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    RootComponent = CreateDefaultSubobject<USceneComponent>("Comp");    
}

// Called when the game starts or when spawned
void ATestActor::BeginPlay() {
    Super::BeginPlay();

    // Enable File Logging
    UDebugLib::SetLogFileNameAndDir(TEXT("Test.log"), TEXT("Logs")); // Needs to be called before enabling the logging or delete the old file manually.
    UDebugLib::SetLoggingToFile(true); // Best to be called from UGameInstance::Init()

    // Macros
    LOG("LOG");
    LOG_INT("Int", 7);
    LOG_BOOL("Bool", true);
    LOG_FLOAT("Float", 7.7777f);
    LOG_VECTOR("Vector", FVector(1,2,3));
    LOG_STRING("String", TEXT("MyString"));
    
    FString Temp = TEXT("Hello");    
    LOGF( TEXT("Format Text: Int: %d, Float %f, String: %s"), 1, 1.9f, *Temp);
    LOGF_WARNING( TEXT("Format Text: Int: %d, Float %f, String: %s"), 1, 1.9f, *Temp);
    LOGF_ERROR( TEXT("Format Text: Int: %d, Float %f, String: %s"), 1, 1.9f, *Temp);    
    
    LOG_WARNING("Log Warning");
    LOG_ERROR("Log Error");   

    // Custom log channels
    LOG_CHANNEL("Message in a custom log channel", LogCustom);
    LOG_CHANNEL_WARNING("Warning in a custom log channel", LogCustom);
    LOG_CHANNEL_ERROR("Error in a custom log channel", LogCustom);
    
    //UE_LOGFMT() // TODO: Can we use this for the LOGF

    UDebugLib::VLog("On screen");
    UDebugLib::VLogInt("Int on screen", 7);

    FVector Start = GetActorLocation();
    FVector End = Start + GetActorForwardVector() * 500.0f;
    FHitResult Hit;    
    
    // Debug Line Traces
    if(GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECC_Visibility)) {
        LOG("TRACE HIT");
    }

    UDebugLib::DrawDebugLineTrace(GetWorld(), Start, End, Hit, 10);

    // Debug Sphere Traces
    Start = GetActorLocation() + GetActorRightVector() * 200.0f;
    End = Start + GetActorForwardVector() * 500.0f;
    const float Radius = 50.0f;

    if(GetWorld()->SweepSingleByChannel(Hit, Start, End, FQuat::Identity,  ECC_Visibility, FCollisionShape::MakeSphere(Radius))) {
        LOG("SPHERE SWEEP HIT");
    }    

    UDebugLib::DrawDebugSphereTrace(GetWorld(), Start, End, Hit, Radius, 10);

    // Debug Box Traces
    Start = GetActorLocation() + GetActorRightVector() * 400.0f;
    End = Start + GetActorForwardVector() * 500.0f;
    const FVector HalfExtent = FVector(50);
    
    if(GetWorld()->SweepSingleByChannel(Hit, Start, End, FQuat::Identity,  ECC_Visibility, FCollisionShape::MakeBox(HalfExtent))) {
        LOG("BOX SWEEP HIT");
    }    

    UDebugLib::DrawDebugBoxTrace(GetWorld(), Start, End, Hit, HalfExtent, FRotator::ZeroRotator, 10);        
}

// Called every frame
void ATestActor::Tick(float DeltaTime) {
    Super::Tick(DeltaTime);
    FVector Start = GetActorLocation() + GetActorRightVector() * -250.0f;
    FVector End = Start + GetActorForwardVector() * 500.0f;
    FHitResult Hit;    
    const FVector HalfExtent = FVector(100);
    
    if(GetWorld()->SweepSingleByChannel(Hit, Start, End, FQuat::Identity,  ECC_Visibility, FCollisionShape::MakeBox(HalfExtent))) {
        
    }    

    UDebugLib::DrawDebugBoxTrace(GetWorld(), Start, End, Hit, HalfExtent, FRotator::ZeroRotator, 0);    
}
