// Copyright Epic Games, Inc. All Rights Reserved.

#include "MyPlugins.h"
#include "Modules/ModuleManager.h"

DEFINE_LOG_CATEGORY(LogCustom);

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, MyPlugins, "MyPlugins" );
