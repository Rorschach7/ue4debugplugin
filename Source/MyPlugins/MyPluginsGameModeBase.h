// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MyPluginsGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MYPLUGINS_API AMyPluginsGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
