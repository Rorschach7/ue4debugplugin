// Fill out your copyright notice in the Description page of Project Settings.


#include "FileLogger.h"
#include "HAL/PlatformFilemanager.h"
#include "GenericPlatform/GenericPlatformFile.h"
#include "Misc/FileHelper.h"

bool FileLogger::bLogToFile = false;
FString FileLogger::LogFileName = TEXT("DebugLog.log");

FileLogger::FileLogger() {}

FileLogger::~FileLogger() {}

void FileLogger::LogToFile(const FString& Msg, const FString& Type) {

	// Only use this in shipping builds and with file logging enabled
	if(!UE_BUILD_SHIPPING || !bLogToFile) {
		return;
	}	

	// Log file path: C:\Users\*user*\AppData\Local\ProjectName\DebugLog.log
	FString LogDir = FString(FPlatformProcess::UserSettingsDir());
	const FString LogFilePath = GetLogFilePath();
		
	FString DateString = FDateTime::Now().ToString(TEXT("%d.%m.%Y-%H:%M:%S"));
	FString Line = "[" + DateString + "] " + Type + " " + Msg;

	FString Output = Line + LINE_TERMINATOR;

	FFileHelper::SaveStringToFile(Output, *LogFilePath, FFileHelper::EEncodingOptions::AutoDetect, &IFileManager::Get(), EFileWrite::FILEWRITE_Append);
}

void FileLogger::LogError(const FString& Msg) {
	LogToFile(Msg, TEXT("[ERROR]")); 
}

void FileLogger::LogWarning(const FString& Msg) {
	LogToFile(Msg, TEXT("[WARNING]"));
}

void FileLogger::DeleteLog() {
	FString LogDir = FString(FPlatformProcess::UserSettingsDir());
	const FString LogFilePath = GetLogFilePath();
	
	if (FPaths::ValidatePath(LogFilePath) && FPaths::FileExists(LogFilePath)) {
		IFileManager& FileManager = IFileManager::Get();
		FileManager.Delete(*LogFilePath);
	}
	
}

FString FileLogger::GetLogFilePath() {
	FString LogDir = FString(FPlatformProcess::UserSettingsDir());
	if(FApp::HasProjectName()) {
		return FPaths::Combine(LogDir, FApp::GetProjectName(), LogFileName);
	}
	return FPaths::Combine(LogDir, LogFileName);
}
