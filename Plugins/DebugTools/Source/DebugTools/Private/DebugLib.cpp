// Copyright 2020 Kevin Lichtenberg. All Rights Reserved.


#include "DebugLib.h"
#include "DrawDebugHelpers.h"
#include "Engine/Engine.h"
#include "DebugTools.h"
#include "FileLogger.h"

static const FLinearColor TraceColor = FLinearColor::Red;
static const FLinearColor TraceHitColor = FLinearColor::Green;
static const float TraceImpactPointSize = 16.0f;

void UDebugLib::LogF_Internal(const FString& Message, const int32 Verbosity) {
	if (UE_BUILD_SHIPPING && FileLogger::bLogToFile) {
		FileLogger::LogToFile(Message, TEXT(""));
	}

	if(Verbosity == 0) {
		UE_LOG(LogDebug, Display, TEXT("%s"), *Message);		
	} else if(Verbosity == 1) {
		UE_LOG(LogDebug, Warning, TEXT("%s"), *Message);
	} else {
		UE_LOG(LogDebug, Error, TEXT("%s"), *Message);
	}
}

void UDebugLib::Log_Internal(const FString& Message, const FLogCategoryBase& Channel) {
	if (UE_BUILD_SHIPPING && FileLogger::bLogToFile) {
		FileLogger::LogToFile(Message, TEXT(""));
	}
	
	UE_LOG_REF(Channel, Display, TEXT("%s"), *Message);
}

void UDebugLib::Log_InternalWarning(const FString& Message, const FLogCategoryBase& Channel) {
	if (UE_BUILD_SHIPPING && FileLogger::bLogToFile) {
		FileLogger::LogWarning(Message);
	}
	
	UE_LOG_REF(Channel, Warning, TEXT("%s"), *Message);
}

void UDebugLib::Log_InternalError(const FString& Message, const FLogCategoryBase& Channel) {
	if (UE_BUILD_SHIPPING && FileLogger::bLogToFile) {
		FileLogger::LogError(Message);
	}
	UE_LOG_REF(Channel, Error, TEXT("%s"), *Message);
}

void UDebugLib::SetLoggingToFile(bool bEnable) {
	FileLogger::bLogToFile = true;
	FileLogger::DeleteLog();
}

void UDebugLib::SetLogFileNameAndDir(const FString FileName, const FString DirName) {
	if (!FileName.IsEmpty()) {
		if (DirName.IsEmpty()) {
			FileLogger::LogFileName = FileName;
			return;
		}
		FString Path = FPaths::Combine(DirName, FileName);
	}
	LOG_ERROR("Could not set new log file name because filename is empty!");
}

void UDebugLib::Log(const FString& Message, const EDebugLogChannel Channel) {
	switch (Channel) {
		case EDebugLogChannel::GENERAL:
			Log_Internal(Message, LogGeneral);
			break;
		case EDebugLogChannel::INVENTORY:
			Log_Internal(Message, LogInventory);
			break;
		case EDebugLogChannel::AI:
			Log_Internal(Message, LogAI);
			break;
		case EDebugLogChannel::SAVE_SYSTEM:
			Log_Internal(Message, LogSaveSystem);
			break;
		case EDebugLogChannel::UI:
			Log_Internal(Message, LogUI);
			break;
		case EDebugLogChannel::VR:
			Log_Internal(Message, LogVR);
			break;
		case EDebugLogChannel::NETWORK:
			Log_Internal(Message, LogNetwork);
			break;
		case EDebugLogChannel::DEBUG:
			Log_Internal(Message, LogDebug);
			break;
		default:
			Log_Internal(Message, LogDebug);		
	}
}

void UDebugLib::LogWarning(const FString& Message, const EDebugLogChannel Channel) {
	switch (Channel) {
		case EDebugLogChannel::GENERAL:
			Log_InternalWarning(Message, LogGeneral);
			break;
		case EDebugLogChannel::INVENTORY:
			Log_InternalWarning(Message, LogInventory);
			break;
		case EDebugLogChannel::AI:
			Log_InternalWarning(Message, LogAI);
			break;
		case EDebugLogChannel::SAVE_SYSTEM:
			Log_InternalWarning(Message, LogSaveSystem);
			break;
		case EDebugLogChannel::UI:
			Log_InternalWarning(Message, LogUI);
			break;
		case EDebugLogChannel::VR:
			Log_InternalWarning(Message, LogVR);
			break;
		case EDebugLogChannel::NETWORK:
			Log_InternalWarning(Message, LogNetwork);
			break;
		case EDebugLogChannel::DEBUG:
			Log_InternalWarning(Message, LogDebug);
			break;
		default:
			Log_InternalWarning(Message, LogDebug);
	}	
}

void UDebugLib::LogError(const FString& Message, const EDebugLogChannel Channel) {
	switch (Channel) {
		case EDebugLogChannel::GENERAL:
			Log_InternalError(Message, LogGeneral);
			break;
		case EDebugLogChannel::INVENTORY:
			Log_InternalError(Message, LogInventory);
			break;
		case EDebugLogChannel::AI:
			Log_InternalError(Message, LogAI);
			break;
		case EDebugLogChannel::SAVE_SYSTEM:
			Log_InternalError(Message, LogSaveSystem);
			break;
		case EDebugLogChannel::UI:
			Log_InternalError(Message, LogUI);
			break;
		case EDebugLogChannel::VR:
			Log_InternalError(Message, LogVR);
			break;
		case EDebugLogChannel::NETWORK:
			Log_InternalError(Message, LogNetwork);
			break;
		case EDebugLogChannel::DEBUG:
			Log_InternalError(Message, LogDebug);
			break;
		default:
			Log_InternalError(Message, LogDebug);
	}
}

void UDebugLib::LogBool(const FString& Message, const bool LogData, const EDebugLogType LoggingType,
                        const EDebugLogChannel Channel) {
	FString BoolSting = FString::Printf(TEXT("FALSE"));
	if (LogData) {
		BoolSting = FString::Printf(TEXT("TRUE"));
	}

	const FString Temp = FString::Printf(TEXT("%s: %s"), *Message, *BoolSting);

	switch (LoggingType) {
		case EDebugLogType::NORMAL:
			Log(Temp, Channel);
			break;
		case EDebugLogType::WARNING:
			LogWarning(Temp, Channel);
			break;
		case EDebugLogType::ERROR:
			LogError(Temp, Channel);
			break;
		default:
			break;
	}

	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, -1, FColor::Red, Temp);
	}
}

void UDebugLib::LogString(const FString& Message, const FString LogData, const EDebugLogType LoggingType,
                          const EDebugLogChannel Channel) {
	const FString Temp = FString::Printf(TEXT("%s: %s"), *Message, *LogData);

	switch (LoggingType) {
		case EDebugLogType::NORMAL:
			Log(Temp, Channel);
			break;
		case EDebugLogType::WARNING:
			LogWarning(Temp, Channel);
			break;
		case EDebugLogType::ERROR:
			LogError(Temp, Channel);
			break;
		default:
			break;
	}

	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, -1, FColor::Red, Temp);
	}
}

void UDebugLib::LogInt(const FString& Message, const int32 LogData, const EDebugLogType LoggingType,
                       const EDebugLogChannel Channel) {
	const FString Temp = FString::Printf(TEXT("%s: %d"), *Message, LogData);

	switch (LoggingType) {
		case EDebugLogType::NORMAL:
			Log(Temp, Channel);
			break;
		case EDebugLogType::WARNING:
			LogWarning(Temp, Channel);
			break;
		case EDebugLogType::ERROR:
			LogError(Temp, Channel);
			break;
		default:
			break;
	}

	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, -1, FColor::Red, Temp);
	}
}

void UDebugLib::LogInt64(const FString& Message, const int64 LogData, const EDebugLogType LoggingType,
                         const EDebugLogChannel Channel) {
	const FString Temp = FString::Printf(TEXT("%s: %lld"), *Message, LogData);

	switch (LoggingType) {
		case EDebugLogType::NORMAL:
			Log(Temp, Channel);
			break;
		case EDebugLogType::WARNING:
			LogWarning(Temp, Channel);
			break;
		case EDebugLogType::ERROR:
			LogError(Temp, Channel);
			break;
		default:
			break;
	}

	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, -1, FColor::Red, Temp);
	}
}

void UDebugLib::LogFloat(const FString& Message, const float LogData, const EDebugLogType LoggingType,
                         const EDebugLogChannel Channel) {
	const FString Temp = FString::Printf(TEXT("%s: %f"), *Message, LogData);

	switch (LoggingType) {
		case EDebugLogType::NORMAL:
			Log(Temp, Channel);
			break;
		case EDebugLogType::WARNING:
			LogWarning(Temp, Channel);
			break;
		case EDebugLogType::ERROR:
			LogError(Temp, Channel);
			break;
		default:
			break;
	}

	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, -1, FColor::Red, Temp);
	}
}

void UDebugLib::LogVector(const FString& Message, const FVector& LogData, EDebugLogType LoggingType,
                          EDebugLogChannel Channel) {
	const FString Temp = FString::Printf(TEXT("%s: %s"), *Message, *LogData.ToString());

	switch (LoggingType) {
		case EDebugLogType::NORMAL:
			Log(Temp, Channel);
			break;
		case EDebugLogType::WARNING:
			LogWarning(Temp, Channel);
			break;
		case EDebugLogType::ERROR:
			LogError(Temp, Channel);
			break;
		default:
			break;
	}

	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, -1, FColor::Red, Temp);
	}
}

void UDebugLib::VLog(const FString& Message, float DisplayLength) {
	Log(Message, EDebugLogChannel::GENERAL);
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, DisplayLength, FColor::White, Message);
	}
}

void UDebugLib::VLogWaring(const FString& Message, float DisplayLength) {
	LogWarning(Message, EDebugLogChannel::VR);
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, DisplayLength, FColor::Yellow, Message);
	}
}

void UDebugLib::VLogError(const FString& Message, const float DisplayLength) {
	LogError(Message, EDebugLogChannel::VR);
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, DisplayLength, FColor::Red, Message);
	}
}

void UDebugLib::VLogString(const FString& Message, const FString& LogData, float DisplayTime,
                           EDebugLogType LoggingType) {
	const FString Temp = FString::Printf(TEXT("%s: %s"), *Message, *LogData);

	switch (LoggingType) {
		case EDebugLogType::NORMAL:
			VLog(Temp, DisplayTime);
			break;
		case EDebugLogType::WARNING:
			VLogWaring(Temp, DisplayTime);
			break;
		case EDebugLogType::ERROR:
			VLogError(Temp, DisplayTime);
			break;
		default:
			break;
	}
}

void UDebugLib::VLogInt(const FString& Message, const int32 LogData, float DisplayTime, EDebugLogType LoggingType) {
	const FString Temp = FString::Printf(TEXT("%s: %d"), *Message, LogData);

	switch (LoggingType) {
		case EDebugLogType::NORMAL:
			VLog(Temp, DisplayTime);
			break;
		case EDebugLogType::WARNING:
			VLogWaring(Temp, DisplayTime);
			break;
		case EDebugLogType::ERROR:
			VLogError(Temp, DisplayTime);
			break;
		default:
			break;
	}
}

void UDebugLib::VLogFloat(const FString& Message, const float LogData, float DisplayTime,
                          EDebugLogType LoggingType) {
	const FString Temp = FString::Printf(TEXT("%s: %f"), *Message, LogData);

	switch (LoggingType) {
		case EDebugLogType::NORMAL:
			VLog(Temp, DisplayTime);
			break;
		case EDebugLogType::WARNING:
			VLogWaring(Temp, DisplayTime);
			break;
		case EDebugLogType::ERROR:
			VLogError(Temp, DisplayTime);
			break;
		default:
			break;
	}
}

void UDebugLib::VLogVector(const FString& Message, const FVector& LogData, float DisplayTime,
                           EDebugLogType LoggingType) {
	const FString Temp = FString::Printf(TEXT("%s: %s"), *Message, *LogData.ToString());

	switch (LoggingType) {
		case EDebugLogType::NORMAL:
			VLog(Temp, DisplayTime);
			break;
		case EDebugLogType::WARNING:
			VLogWaring(Temp, DisplayTime);
			break;
		case EDebugLogType::ERROR:
			VLogError(Temp, DisplayTime);
			break;
		default:
			break;
	}
}

void UDebugLib::DrawDebugSweptSphere(UWorld* World, FVector const& Start, FVector const& End, const float Radius,
                                     FColor const& Color, const bool bPersistentLines, const float LifeTime, const uint8 DepthPriority) {
	FVector const TraceVec = End - Start;
	float const Dist = TraceVec.Size();

	FVector const Center = Start + TraceVec * 0.5f;
	float const HalfHeight = (Dist * 0.5f) + Radius;

	FQuat const CapsuleRot = FRotationMatrix::MakeFromZ(TraceVec).ToQuat();
	DrawDebugCapsule(World, Center, HalfHeight, Radius, CapsuleRot, Color, bPersistentLines, LifeTime, DepthPriority);
}

void UDebugLib::DrawDebugSweptBox(UWorld* World, FVector const& Start, FVector const& End, const FVector HalfExtent,
                                  const FRotator Orientation, FColor const& Color, const bool bPersistentLines, const float LifeTime,
                                  const uint8 DepthPriority) {
	FVector const TraceVec = End - Start;
	float const Dist = TraceVec.Size();

	FVector const Center = Start + TraceVec * 0.5f;

	FQuat const CapsuleRot = Orientation.Quaternion();
	DrawDebugBox(World, Start, HalfExtent, CapsuleRot, Color, bPersistentLines, LifeTime, DepthPriority);

	//now draw lines from vertices
	FVector Vertices[8];
	Vertices[0] = Start + Orientation.RotateVector(FVector(-HalfExtent.X, -HalfExtent.Y, -HalfExtent.Z)); //flt
	Vertices[1] = Start + Orientation.RotateVector(FVector(-HalfExtent.X, HalfExtent.Y, -HalfExtent.Z)); //frt
	Vertices[2] = Start + Orientation.RotateVector(FVector(-HalfExtent.X, -HalfExtent.Y, HalfExtent.Z)); //flb
	Vertices[3] = Start + Orientation.RotateVector(FVector(-HalfExtent.X, HalfExtent.Y, HalfExtent.Z)); //frb
	Vertices[4] = Start + Orientation.RotateVector(FVector(HalfExtent.X, -HalfExtent.Y, -HalfExtent.Z)); //blt
	Vertices[5] = Start + Orientation.RotateVector(FVector(HalfExtent.X, HalfExtent.Y, -HalfExtent.Z)); //brt
	Vertices[6] = Start + Orientation.RotateVector(FVector(HalfExtent.X, -HalfExtent.Y, HalfExtent.Z)); //blb
	Vertices[7] = Start + Orientation.RotateVector(FVector(HalfExtent.X, HalfExtent.Y, HalfExtent.Z)); //brb
	for (int32 VertexIdx = 0; VertexIdx < 8; ++VertexIdx) {
		DrawDebugLine(World, Vertices[VertexIdx], Vertices[VertexIdx] + TraceVec, Color, bPersistentLines, LifeTime, DepthPriority);
	}

	DrawDebugBox(World, End, HalfExtent, CapsuleRot, Color, bPersistentLines, LifeTime, DepthPriority);
}

void UDebugLib::LogChannel(const FString& Message, const FLogCategoryBase& Channel) {
	Log_Internal(Message, Channel);
}

void UDebugLib::LogChannelWarning(const FString& Message, const FLogCategoryBase& Channel) {
	Log_InternalWarning(Message, Channel);
}

void UDebugLib::LogChannelError(const FString& Message, const FLogCategoryBase& Channel) {
	Log_InternalError(Message, Channel);
}

void UDebugLib::DrawDebugLineTrace(UWorld* World, const FVector& Start, const FVector& End, FHitResult& HitInfo, float LifeTime) {
	const bool bPersistent = false;

	if (HitInfo.bBlockingHit) {
		DrawDebugLine(World, Start, HitInfo.ImpactPoint, TraceColor.ToFColor(true), bPersistent, LifeTime);
		DrawDebugPoint(World, HitInfo.ImpactPoint, 16.0f, TraceColor.ToFColor(true), bPersistent, LifeTime);
		DrawDebugLine(World, HitInfo.ImpactPoint, End, TraceHitColor.ToFColor(true), bPersistent, LifeTime);
	} else {
		DrawDebugLine(World, Start, End, TraceColor.ToFColor(true), bPersistent, LifeTime);
	}
}

void UDebugLib::DrawDebugSphereTrace(UWorld* World, const FVector& Start, const FVector& End, FHitResult& HitInfo, float Radius,
                                     float LifeTime) {
	const bool bPersistent = false;

	if (HitInfo.bBlockingHit) {
		// Red up to the blocking hit, green thereafter
		DrawDebugSweptSphere(World, Start, HitInfo.Location, Radius, TraceColor.ToFColor(true), bPersistent, LifeTime);
		DrawDebugSweptSphere(World, HitInfo.Location, End, Radius, TraceHitColor.ToFColor(true), bPersistent, LifeTime);
		DrawDebugPoint(World, HitInfo.ImpactPoint, TraceImpactPointSize, TraceColor.ToFColor(true), bPersistent, LifeTime);
	} else {
		// no hit means all red
		DrawDebugSweptSphere(World, Start, End, Radius, TraceColor.ToFColor(true), bPersistent, LifeTime);
	}
}

void UDebugLib::DrawDebugCapsuleTrace(UWorld* World, const FVector& Start, const FVector& End, const FHitResult& HitInfo, float Radius, float HalfHeight,
                                      float LifeTime) {
	const bool bPersistent = false;

	if (HitInfo.bBlockingHit) {
		// Red up to the blocking hit, green thereafter
		DrawDebugCapsule(World, Start, HalfHeight, Radius, FQuat::Identity, TraceColor.ToFColor(true), bPersistent, LifeTime);
		DrawDebugCapsule(World, HitInfo.Location, HalfHeight, Radius, FQuat::Identity, TraceColor.ToFColor(true), bPersistent, LifeTime);
		DrawDebugLine(World, Start, HitInfo.Location, TraceColor.ToFColor(true), bPersistent, LifeTime);
		DrawDebugPoint(World, HitInfo.ImpactPoint, TraceImpactPointSize, TraceColor.ToFColor(true), bPersistent, LifeTime);

		DrawDebugCapsule(World, End, HalfHeight, Radius, FQuat::Identity, TraceHitColor.ToFColor(true), bPersistent, LifeTime);
		DrawDebugLine(World, HitInfo.Location, End, TraceHitColor.ToFColor(true), bPersistent, LifeTime);
	} else {
		// no hit means all red
		DrawDebugCapsule(World, Start, HalfHeight, Radius, FQuat::Identity, TraceColor.ToFColor(true), bPersistent, LifeTime);
		DrawDebugCapsule(World, End, HalfHeight, Radius, FQuat::Identity, TraceColor.ToFColor(true), bPersistent, LifeTime);
		DrawDebugLine(World, Start, End, TraceColor.ToFColor(true), bPersistent, LifeTime);
	}
}

void UDebugLib::DrawPoint(UWorld* World, const FVector& Location, const float LifeTime) {
	DrawDebugPoint(World, Location, 16.0f, FColor::Red, false, LifeTime);
}

void UDebugLib::DrawString(UWorld* World, const FVector& Location, const FString& Text, const FColor& Color, const float LifeTime) {
	DrawDebugString(World, Location, Text, nullptr, Color, LifeTime);
}

void UDebugLib::DrawSphere(UWorld* World, const FVector& Location, const float Radius, const float LifeTime) {
	const bool bPersistent = false;
	DrawDebugSphere(World, Location, Radius, 16.0f, FColor::Red, bPersistent, LifeTime);
}

void UDebugLib::DrawBox(UWorld* World, const FVector& Location, const FRotator& Orientation, const FVector& HalfExtent, const float LifeTime) {
	const bool bPersistent = false;
	DrawDebugBox(World, Location, HalfExtent, FColor::Red, bPersistent, LifeTime);
}

void UDebugLib::DrawDebugBoxTrace(UWorld* World, const FVector& Start, const FVector& End, FHitResult& HitInfo, const FVector& HalfExtent,
                                  const FRotator& Orientation, const float LifeTime) {
	const bool bPersistent = false;

	if (HitInfo.bBlockingHit) {
		// Red up to the blocking hit, green thereafter
		DrawDebugSweptBox(World, Start, HitInfo.Location, HalfExtent, Orientation, TraceColor.ToFColor(true), bPersistent, LifeTime);
		DrawDebugSweptBox(World, HitInfo.Location, End, HalfExtent, Orientation, TraceHitColor.ToFColor(true), bPersistent, LifeTime);
		DrawDebugPoint(World, HitInfo.ImpactPoint, TraceImpactPointSize, TraceColor.ToFColor(true), bPersistent, LifeTime);
	} else {
		// no hit means all red
		DrawDebugSweptBox(World, Start, End, HalfExtent, Orientation, TraceColor.ToFColor(true), bPersistent, LifeTime);
	}
}
