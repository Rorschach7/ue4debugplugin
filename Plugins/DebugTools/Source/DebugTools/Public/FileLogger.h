// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class DEBUGTOOLS_API FileLogger {
	
public:

	static bool bLogToFile;
	static FString LogFileName;
		
	FileLogger();
	~FileLogger();

	static void LogToFile(const FString& Msg, const FString& Type = TEXT(""));
	static void LogWarning(const FString& Msg);
	static void LogError(const FString& Msg);

	static void DeleteLog();

private:

	static FString GetLogFilePath();
	
};
