# UE4DebugPlugin

A plugin for Unreal Engine 5 providing easy to use logging and other debugging functionality.

It mainly provides easy to use functions/macros for already existing functionaliy.

# How To

The blueprint functions can be used as soon as the plugin is enabled.
To use the macros and functions in your code you have to include the DebugLib.h file.
In order for the compiler to actually find that header file see the two aprroaches below.

## Project based

Copy the repo into the Plugins folder of your project.

Add the plugin to your build file by adding the following:

PublicDependencyModuleNames.AddRange(new string[] { "...", "DebugTools" });

## Unreal Marketplace

Download the plugin from the Unreal Marketplace: [here](https://www.unrealengine.com/marketplace/en-US/product/log-debug-tools)

Enable the plugin via the editor's plugin manager

In order to use the plugin's functions in your code you need to modify your uproject file.

Add Debug Tools to the "AdditionalDependencies".

Finally regenerate your project files.

Your uproject file should look something like this:

```json
{
	"FileVersion": 3,
	"EngineAssociation": "4.25",
	"Category": "",
	"Description": "",
	"Modules": [
		{
			"Name": "MyProject",
			"Type": "Runtime",
			"LoadingPhase": "Default",
			"AdditionalDependencies": [
				"Engine",
				"DebugTools" // <===== This is important if you you want to use the plugin in your code
			]
		}
	],
	"Plugins": [
		{
			"Name": "DebugTools",
			"Enabled": true
		}
	]
}
```

# Features

All of the plugins function are part of the UDebugLib class.
All functions are documented. While some functionality is also exposed to blueprint this plugin is mainly focused on c++ logging/debugging.

## Logging Macros

Multiple Logging Macros that allow simple logging of primitive data types:

```cpp
LOG("LOG");
LOG_INT("Int", 7); // "Int : 7"
LOG_BOOL("Bool", true); // "Bool : true"
LOG_FLOAT("Float", 7.7777f); // "Float : 7.7777f"
LOG_VECTOR("Vector", FVector(1,2,3)); // ...
LOG_STRING("String", TEXT("MyString")); // ...

FString Temp = TEXT("Hello");    
LOGF( TEXT("Format Text: Int: %d, Float %f, String: %s"), 1, 1.9f, *Temp);

LOG_WARNING("Log Warning"); // Logs with warning verbosity
LOG_ERROR("Log Error"); // Logs with error verbosity
```

Every macro also display the class name and the line number it was called from:

`LOG_STRING("Actor Name", *this->GetName()); // "LogGeneral: Display: Actor Name : BP_TargetDummy2 [AMyActor::637]"`

## More Log Channels

The plugin also adds multiple new log categories:

- General
- Inventory
- AI
- SaveSystem
- UI
- VR
- Network
 
These can be used to filter the output log for specific categories.

You can log to a one of the categories using the EDebugLogChannel enum the following macros:

```cpp
LOG_CHANNEL(Message, Channel)               
LOG_CHANNEL_WARNING(Message, Channel)       
LOG_CHANNEL_ERROR(Message, Channel)

// Example
LOG_CHANNEL("My Message", EDebugLogChannel::NETWORK);
```

### Loggin to custom log file in a shipping build (only 4.27 and higher)
In version 1.2 you can automatically write all logs to a custom log file.
To enable this feature use the following line of code.

```cpp
UDebugLib::SetLoggingToFile(true); // Best to be called from UGameInstance::Init()
```

The default log file will be created in ...\AppData\Local\"ProjectName" and is called "DebugLog.log".

You can also change the log file name.
```cpp
FileLogger::LogFileName = TEXT("MyCustomLogFileName.log");
```

### Logging formatted strings (only 5.1 and higher)

LOGF(TEXT("Formatted string %d and %f"), 1, 1.5f);

Works like FString::Printf or similar print functions.

## Debug Traces

This plugin provides functions to display line, sphere and box traces and their intersections similar to the blueprint trace functions.


DrawDebugLineTrace:

```cpp
const FVector Start = GetActorLocation();
const FVector End = Start + GetActorForward() * 500.0f;
FHitResult Hit;

if (GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECC_Visibility)) {
    // do something ...
}
// Draw the line trace
UDebugLib::DrawDebugLineTrace(GetWorld(), Start, End, Hit, 10.0f);
```

More features will be added in the future.

## Build Plugin 
C:\Program Files\Epic Games\UE_4.26\Engine\Build\BatchFiles RunUAT.bat BuildPlugin -Plugin="C:/Users/kevli/Documents/Unreal Projects/ue4debugplugin/Plugins/DebugTools/DebugTools.uplugin" -Package=C:/Users/kevli/Desktop/DebugTools -CreateSubFolder -Package=\out -VS2019
C:\Program Files\Epic Games\UE_5.0EA\Engine\Build\BatchFiles RunUAT.bat BuildPlugin -Plugin="C:/Users/kevli/Documents/Unreal Projects/ue4debugplugin/Plugins/DebugTools/DebugTools.uplugin" -Package=C:/Users/kevli/Desktop/DebugTools -CreateSubFolder -Package=\out -VS2019
